const { google } = require('googleapis');
const parseMessage = require('gmail-api-parse-message');
const { spawn } = require('child_process');
const signature = require('email-signature-detector');
const {PythonShell} = require('python-shell');


exports.getOAuthClient = function(ClientId, ClientSecret, RedirectionUrl) {
    return new google.auth.OAuth2(ClientId, ClientSecret, RedirectionUrl);
}

/**
 * Generate a url that asks permissions for Google+ and Gmail scopes
 *
 * @param ClientId server google Id
 * @param ClientSecret server google secret
 * @param RedirectionUrl redirect url after log in
 *
 *
 */
exports.getAuthUrl = function (ClientId, ClientSecret, RedirectionUrl) {
    var oauth2Client = exports.getOAuthClient(ClientId, ClientSecret, RedirectionUrl);
    // Google+ and Gmail scopes
    var scopes = [
        'https://www.googleapis.com/auth/plus.me',
        'https://www.googleapis.com/auth/gmail.readonly'
    ];

    var url = oauth2Client.generateAuthUrl({
        access_type: 'online',
        scope: scopes // If you only need one scope you can pass it as string
    });

    return url;
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
exports.listLabels = function(auth) {
    const gmail = google.gmail({ version: 'v1', auth });
    return new Promise((resolve, reject) => {
        gmail.users.labels.list({
            userId: 'me',
        }, (err, res) => {
            if (err) {
                console.log('The API returned an error: ' + err);
                reject(err);
            }
            var l = res.data.labels;
            if (l.length > 0) {
                console.log('labels found.', l);
                var data = ''
            l.forEach(element => {
                data += "<li>" + element.name + "</li>"
            });
            resolve(data);
            } else {
                console.log('No labels found.');
                reject('No labels found');
            }
        });
    });
}
/**
 * Get user's google profile info
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
exports.getProfile = async function (auth) {
    const gmail = google.gmail({ version: 'v1', auth });
    return new Promise((resolve, reject) => {
        gmail.users.getProfile({
            auth: auth,
            userId: 'me'
        }, function (err, res) {
            if (err) {
                reject(err);
            } else {
               resolve(res);
            }
        });
    });
}

/**
 * Lists the messages in the user's account from the specified label.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 * @param label A label which contains emails
 * @param max Number of eails to retreive
 * @param full Boolean, true if to return all information of the emails, false to return just emails text
 */
exports.listMessages = async function(auth, label, max, full) {
    const gmail = google.gmail({ version: 'v1', auth });
    return new  Promise((resolve, reject) => {
        var options = {
            userId: 'me',
            labelIds: label,
            maxResults: max
        }
        if(max == 0){
            var options = {
                userId: 'me',
                labelIds: label,
            }
        }
        gmail.users.messages.list(options, (err, res) => {
            if (err) {
                console.log('The API returned an error: ' + err);
                reject(err);
            }
            var messages = res.data.messages;
            if (messages.length > 0) {
                console.log('MessageIds: ', messages);

                var promiseArray = [];
                for (let i = 0; i < messages.length; i++) {
                    const element = messages[i];
                    promiseArray.push(exports.getMessage(auth, element.id, full));
                }
                const request = async () => {
                    const response = await Promise.all(promiseArray);
                    console.log(response);
                    resolve(response);
                }
                request()
            }
        });
    });
}
/**
 * Save in a file messages from the user's account from the specified label.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 * @param label A label which contains emails
 * @param max Number of eails to retreive
 * @param full Boolean, true if to return all information of the emails, false to return just emails text
 * @param user_id identifier for a user, will be used in the filename as  './Storage/'+user_id+'_mail.json'
 */
exports.saveMessages = async function (auth, label, max, full, user_id) {
    return new Promise((resolve,reject) => {
        var promiseMails = exports.listMessages(auth, label, max, full);
        promiseMails.then(function (messages) {
            var messageDict = []
            for (var i = 0; i < messages.length; i++) {
                let from = exports.extractNameMail(messages[i].headers.from);
                let subject = messages[i].headers.subject;
                let textPlain = subject + " " + messages[i].textPlain;
                if (textPlain != null && !IncludeMult(from.adress, ["reply", "subscription", "nepasrepondre", "news", "subsciption", "abonnement", "notification", "actu."])){
                    let message = {'text': textPlain, 'email':from};
                    messageDict.push(message);
                }
            }
            const fs = require('fs');
            var filename = './Storage/'+user_id+'_mail.json'
            fs.writeFile(filename,JSON.stringify(messageDict), function (err) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                console.log("The file was saved!");
                console.log("***Filename:", filename);
                resolve(filename);
            }); 
        });
    });
}
/**
 * Save in a file messages from the user's account from the specified label and run a python program which extract keywords, signature and potential job titles
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 * @param label A label which contains emails
 * @param max Number of eails to retreive
 * @param full Boolean, true if to return all information of the emails, false to return just emails text
 * @param user_id identifier for a user, will be used in the filename as  './Storage/'+user_id+'_mail.json'
 */
exports.ListMessagesSign = async function (auth, label, max, user_id) {
    return new Promise((resolve, reject) => {
        let prom = exports.saveMessages(auth,label,max,true, user_id);
        prom.then(function(filename){
            console.log('*******Filename:', filename);
            console.log('*******user_id:', user_id);
            
            let runPy = new Promise(function (success, nosuccess) {
                let pyprog = spawn('python3', ['./Python/processMails.py', user_id]);

                pyprog.stdout.on('data', function (data) {
                    //messages[i].signature = JSON.parse(data.toString('utf8')).sign;
                    console.log('Filename', data.toString());
                    success(data.toString());
                }); 
                pyprog.stderr.on('data', (data) => {
                    //nosuccess(data.toString());
                });
            });
            runPy.then(function(data){
                resolve(filename);
            }, function(err){
                reject(err);
            })
        }, function(err){
            reject(err);
        })
    });
}

/**
 * Retreive a message identified by messageId from user gmail account
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 * @param messageId mail ID 
 * @param full Boolean, true to return all information, false to return just email text
 */
exports.getMessage = function(auth, messageId, full) {
    const gmail = google.gmail({ version: 'v1', auth });
    
    return new Promise((resolve, reject) => {
        gmail.users.messages.get({
            userId: 'me',
            id: messageId
        }, (err, res) => {
                if (err) {
                    console.log('The API returned an error: ' + err);
                    reject(err);
                }
                else{
                    message = parseMessage(res.data);
                    if(!full){
                        resolve(message.textPlain);
                    }
                    else{
                        //{ email: 'rons@acme.com', displayName: 'Ron Smith' };
                        resolve(message);
                        //var sig = signature.getSignature(message.textPlain, from);
                    }
                }               
            });
        });
}

/**
 * Return a dict containing adress and name from a string "Jean Test <jean.test@gmail.com>"
 *
 * @param str adress + name as "name <adress>"
 */
exports.extractNameMail= function(str){
    var offset1 = str.indexOf('<');
    var offset2 = str.indexOf('>');
    var name = str.substring(0,offset1);
    var adress = str.substring(offset1+1, offset2);
    return { 'adress': adress, displayName: name };
}

/**
 * Return true if email contains one of the string in NotUsefull
 *
 * @param email An email adress
 * @param NotUsefull An array of words to search in email
 */
function IncludeMult(email, NotUsefull){
    var bool = false;
    NotUsefull.forEach( (element) => {
        bool = bool || email.includes(element);
    })
    return bool;
}