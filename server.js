var http = require('http');
var express = require('express');
var Gmail = require('./gmail');
var Session = require('express-session');
const fs = require('fs');
const { google } = require('googleapis');
var plus = google.plus_v1;
const https = require('https')

var parseMessage = require('gmail-api-parse-message');

const ClientId = "1037988002237-35qesirjdn3nor9ilm797uu46sgdsaq8.apps.googleusercontent.com";
const ClientSecret = "LEVj8CFXEYISvnCOwnHBRodm";
const RedirectionUrl = "http://localhost:8090/oauthCallback";

var labels;
var messageList = [];
var app = express();
app.use(Session({
    secret: 'raysources-secret-19890913007',
    resave: false,
    saveUninitialized: false
}));

app.use("/oauthCallback", function (req, res) {
    var oauth2Client = Gmail.getOAuthClient(ClientId, ClientSecret, RedirectionUrl);
    var session = req.session;
    var code = req.query.code;
    oauth2Client.getToken(code, function (err, tokens) {
        // Now tokens contains an access_token and an optional refresh_token. Save them.
        if (!err) {
            oauth2Client.setCredentials(tokens);
            session["tokens"] = tokens;
            res.send(`
            <h3>Login successful!!</h3>
            <a href="/labels">Go to labels page</a>
            <a href="/messages">Go to messages page</a>
            <a href="/details">Go to details page</a>
        `);
        }
        else {
            res.send(`
            <h3>Login failed!!</h3>
        `);
        }
    });
});

app.use("/details", function (req, res) {
    var oauth2Client = Gmail.getOAuthClient(ClientId, ClientSecret, RedirectionUrl);
    oauth2Client.setCredentials(req.session["tokens"]);
    var promiseProfile = Gmail.getProfile(oauth2Client);
    promiseProfile.then( function(data) {
        console.log(data.data);
        res.send(data);
    })
});

app.use("/labels", function (req, res) {
    var oauth2Client = Gmail.getOAuthClient(ClientId, ClientSecret, RedirectionUrl);
    oauth2Client.setCredentials(req.session["tokens"]);
    //const gmail = google.gmail({ version: 'v1', auth: oauth2Client });
    var promiseLabels = Gmail.listLabels(oauth2Client, res);
    promiseLabels.then(function(data) {
        console.log("Labels: ", data)
        res.send(data);
    });
});

app.use("/messages", function (req, res) {
    var oauth2Client = Gmail.getOAuthClient(ClientId, ClientSecret, RedirectionUrl);
    oauth2Client.setCredentials(req.session["tokens"]);
    //const gmail = google.gmail({ version: 'v1', auth: oauth2Client });    LABEL TEST : "Label_6429405975145244440"
    var promiseProfile = Gmail.getProfile(oauth2Client);
    promiseProfile.then(function (data) {
        //console.log(data.data);
        let prom = Gmail.ListMessagesSign(oauth2Client, "Label_6429405975145244440", 0, data.data.emailAddress);
        prom.then(function(data){
            let rawdata = fs.readFileSync('./Storage/data.txt');
            let mailSign = JSON.parse(rawdata);
            console.log(mailSign);
            res.send(mailSign);
        }, function(err){
            console.log(err);
        });
});         


app.use("/", function (req, res) {
    var url = Gmail.getAuthUrl(ClientId, ClientSecret, RedirectionUrl);
    res.send(`
        <h1>Authentication using google oAuth</h1>
        <a href=${url}>Login</a>
    `)
});

var port = 8090;
var options = {
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
}
var server = http.createServer(app);
https.createServer(options, app).listen(8443);
server.listen(port);
server.on('listening', function () {
    console.log(`listening to ${port}`);
});
