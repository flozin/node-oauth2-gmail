from talon.signature.bruteforce import extract_signature as bruteforce_signature
from talon import signature
import sys
import talon
import json
# don't forget to init the library first
# it loads machine learning classifiers


def getSignatureFromMail(message, email):
	#extract signature if there is -- before
	text, sign = bruteforce_signature(message)
	#If bruteforce_signature didn't work, try the method using machine learning
	if(sign == None):
		talon.init()
		text, sign = signature.extract(message, sender=email['adress'])

	if(sign == None):
		res = {'text': text}
	else:
		res = {'text': text, 'sign': sign}
	return res

def getSignatureFromMails(messagesJson):
		#extract signature if there is -- before
	messages = json.loads(messagesJson)
	res = []
	for e in messages:
		resTemp = {}
		text, sign = bruteforce_signature(e['text'])
		#If bruteforce_signature didn't work, try the method using machine learning
		if(sign == None):
			talon.init()
			text, sign = signature.extract(e['text'], sender=e['adress'])

		if(sign == None):
				sign = e['adress']

		resTemp['text'], resTemp['sign'] = text, sign
		res.append(resTemp)
	return res


"""if __name__ == "__main__":
	args = sys.argv[1:]
	if(len(args) >= 1) :
		body = args[0]
		adress = args[1]
		res = getSignatureFromMail(body, adress)
		print(json.dumps(res))
	exit(0)
	#	sys.stdout.flush()
"""

if __name__ == "__main__":
	args = sys.argv[1:]
	if(len(args) > 0):
		with open(args[0]) as json_file:
			data = json.load(json_file)
			res = []
			for p in data:
					text = p['text']
					email = p['email']
					elemTmp= getSignatureFromMail(text, email)
					elemTmp['email'] = email
					res.append(elemTmp)
					#print('---Text: ', text)
					#print('---Email: ', email)
					#print('------Sign : ', elemTmp['sign'])
					#print('')
					#TODO : faire le system pour reconnaitre les job titles et les mots clefs
			with open('./Storage/data.txt', 'w') as outfile:
    				json.dump(res, outfile)
			print('./Storage/data.txt')
