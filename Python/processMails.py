import nltk
import re
from talon.signature.bruteforce import extract_signature as bruteforce_signature
from talon import signature
import sys
import talon
import json
from sklearn.feature_extraction.text import TfidfVectorizer
from stop_words import get_stop_words
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
from sklearn import metrics
import numpy as np
from nltk.corpus import wordnet
import math
import pandas as pd
import unicodedata
from mysqlToPandas import getDataFrame, insert
from TextRank4Keyword import TextRank4Keyword
from stemmer import cleanSentence, stem

def getSignatureFromMail(message, email):
	#extract signature if there is -- before
	text, sign = bruteforce_signature(message)
	#If bruteforce_signature didn't work, try the method using machine learning
	if(sign == None):
		talon.init()
		text, sign = signature.extract(message, sender=email['adress'])

	if(sign == None):
		sign = email
		res = {'text': text}
	else:
		res = {'text': text, 'sign': sign}
	return res

def findJobInSign(sign, job_titles):
	res = []
	signStemmed = stem(sign.split(' '))
	
	for e in job_titles:
		stemmed = stem(e)
		if stemmed != '' and stemmed in signStemmed:
			res.append(e)
	return res

def removePreviousMessages(text):
	lines = text.splitlines()
	textWithoutOriginal = ''''''
	for line in lines:
						if(len(line) > 0 and line[0] != '>'):
							textWithoutOriginal = textWithoutOriginal + '\n' + line
	return textWithoutOriginal.rstrip().lstrip()


if __name__ == "__main__":
	args = sys.argv[1:]
	if(len(args) > 0):
		user_id = args[0]
		filename = './Storage/'+user_id+'_mail.json'
		with open(filename) as json_file:
			data = json.load(json_file)
			res = []
			for p in data:
					text = removePreviousMessages(p['text'])
					email = p['email']
					elemTmp = getSignatureFromMail(text, email)
					elemTmp['email'] = email
					res.append(elemTmp)
			#with open('./Storage/data.txt', 'w') as outfile:
			#	json.dump(res, outfile)
	query = """Select id, name 
				From `job_titles` """
	dfJobs = getDataFrame(query=query)
	job_titles = dfJobs['name']
	jobs = []
	allKeyWords = {}
	processedEmails = []
	for email in res:
		temp = {}
		text = email['text']
		temp['text'] = text
		print('---------Text:', text)
		print('---------Text:', cleanSentence(text))
		tr4w = TextRank4Keyword()
		tr4w.analyze(cleanSentence(text), candidate_pos=['NOUN', 'PROPN'], window_size=4, lower=False)
		keyWords = tr4w.get_keywords(10)
		temp['keywords'] = keyWords
		for e in keyWords:
			if e in allKeyWords:
				allKeyWords[e]+= 1
			else:
				allKeyWords[e] = 1
		emailJobs = []
		if 'sign' in email:
			print('----------------------------SIGN------------------------------------------')
			emailJobs = findJobInSign(email['sign'], job_titles)
		else:
			emailJobs = findJobInSign(text, job_titles)
		temp['jobs'] = emailJobs
		for e in emailJobs:
			if e not in jobs:
				jobs.append(e)

		print("------------keyWords:", keyWords)
		print('-------------Jobs: ', jobs)
		processedEmails.append(temp)
	
		#input('Continue ? Y/N :')
	filename = './Storage/'+user_id+'_processed.json'
	with open(filename, 'w') as outfile:
			json.dump(processedEmails, outfile)
	
	#Send found jobs to database
	query = """INSERT INTO `jobs_mails`(`Id`, `name`) VALUES (%s, %s)"""
	for j in jobs:
		insert(query=query, values=(user_id, j))
		#infos = findInfoSign(email['sign'])
	resKeys = []
	for i in range(0,len(allKeyWords)):
		maximum = 0
		tmp = ()
		for k,v in allKeyWords.items():
			if v >= maximum:
				tmp = (k,v)
				maximum = v
		resKeys.append(tmp)
		allKeyWords.pop(tmp[0])
	query = """INSERT INTO `tags_mails`(`Id`, `name`) VALUES (%s, %s)"""
	for k,count in resKeys:
		if count > 4:
			insert(query=query, values=(user_id, k))
		#infos = findInfoSign(email['sign'])
	print(resKeys)


