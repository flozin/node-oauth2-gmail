import mysql.connector as sql
import pandas as pd
import re
import os
from dotenv import load_dotenv

load_dotenv(dotenv_path='./.env')

MYSQL_HOST = os.getenv("MYSQL_HOST", 'localhost')
MYSQL_DATABASE = os.getenv("MYSQL_DATABASE", 'NewDataMySphere')
MYSQL_USER = os.getenv("MYSQL_USER", 'root')
MYSQL_PASSWORD = os.getenv("MYSQL_PASSWORD", '010995')


def getDataFrame(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, query=''):
        db_connection = sql.connect(
        host=host, database=database, user=user, password=password)
        df = pd.read_sql(query, con=db_connection)
        return df


def insert(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, query='', values=()):
	db_connection = sql.connect(host=host, database=database, user=user, password=password)
	mycursor = db_connection.cursor()
	mycursor.execute(query, values)
	db_connection.commit()

	print(mycursor.rowcount, ''+str(values)+" record inserted.")


def createTableTrans(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, tableName='tags_trans'):
	db_connection = sql.connect(host=host, database=database, user=user, password=password)
	mycursor = db_connection.cursor()
	query = """CREATE TABLE IF NOT EXISTS """+tableName +""" (
    						id INT,
						name VARCHAR(256) NOT NULL,
						PRIMARY KEY (id)
					)"""
	mycursor.execute(query)


def getTags(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, table='tags_trans', tags=''):
	db_connection = sql.connect(host=host, database=database, user=user, password=password)
	mycursor = db_connection.cursor()
	res = []
	if tags in ('', []):
		return []
	if type(tags) == list :
		ids = re.sub('\[', '(', str(tags))
		ids = re.sub('\]', ')', ids)
		query = """SELECT
						`t`.`name`
				FROM
						`""" + table + """` as `t`
				WHERE
						t.id in """ + ids
	else:
		ids = str(tags)
		query = """SELECT
						`t`.`name`
				FROM
						`""" + table + """` as `t`
				WHERE
						t.id =  """ + ids

	mycursor.execute(query)
	for t in mycursor:
		res.append(re.sub('[(),\']', '', str(t)))
	return res

def getFunctions(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, table='function_translations', functions=''):
	db_connection = sql.connect(
			host=host, database=database, user=user, password=password)
	mycursor = db_connection.cursor()
	res = []
	if functions in ('', []):
		return []
	if type(functions) == list:
		ids = re.sub('\[', '(', str(functions))
		ids = re.sub('\]', ')', ids)
		query = '''SELECT
						`t`.`name`
				FROM
						`''' + table + '''` as `t`
				WHERE
						t.function_id in ''' + ids + '''
						 and locale = "en" '''
						
	else:
		ids = str(functions)
		query = '''SELECT
						`t`.`name`
				FROM
						`''' + table +'''` as `t`
				WHERE
						t.function_id	 =  ''' + ids + ' and locale = \"en\" '
						 

	mycursor.execute(query)
	for t in mycursor:
		res.append(re.sub('[(),\']', '', str(t)))
	return res

def getCompanies(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, table='companies', companies=''):
	db_connection = sql.connect(
			host=host, database=database, user=user, password=password)
	mycursor = db_connection.cursor()
	res = []
	if companies in ('', []):
		return []
	if type(companies) == list:
		ids = re.sub('\[', '(', str(companies))
		ids = re.sub('\]', ')', ids)
		query = """SELECT
						`t`.`name`
				FROM
						`""" + table + """` as `t`
				WHERE
						t.id in """ + ids
	else:
		ids = str(companies)
		query = """SELECT
						`t`.`name`
				FROM
						`""" + table + """` as `t`
				WHERE
						t.id =  """ + ids

	mycursor.execute(query)
	for t in mycursor:
		res.append(re.sub('[(),\']', '', str(t)))
	return res


def getJobs(host=MYSQL_HOST, database=MYSQL_DATABASE, user=MYSQL_USER, password=MYSQL_PASSWORD, table='job_titles_trans', jobs=''):
	db_connection = sql.connect(
            host=host, database=database, user=user, password=password)
	mycursor = db_connection.cursor()
	res = []
	if jobs in ('', []):
		return []
	if type(jobs) == list:
		ids = re.sub('\[', '(', str(jobs))
		ids = re.sub('\]', ')', ids)
		query = """SELECT
						`t`.`name`
				FROM
						`""" + table + """` as `t`
				WHERE
						t.id in """ + ids
	else:
		ids = str(jobs)
		query = """SELECT
						`t`.`name`
				FROM
						`""" + table + """` as `t`
				WHERE
						t.id =  """ + ids

	mycursor.execute(query)
	for t in mycursor:
		res.append(re.sub('[(),\']', '', str(t)))
	return res
