from nltk.stem import SnowballStemmer , PorterStemmer
import unicodedata
import re
from stop_words import get_stop_words
import numpy as np

def cleanSentence(sentence):
	res = ''.join((c for c in unicodedata.normalize('NFD', sentence) if unicodedata.category(c) != 'Mn')).lower()
	#res = str(unicodedata.normalize('NFD', sentence).encode('ascii', 'ignore'))  # remove accent
	res = re.sub(r'<.*?>', ' ', res) #Remove html tags like <p> <\p>
	res = re.sub(r'[<>]', ' ', res) # Remove > <
	res = re.sub(r"http\S+", " ", res) #Remove URL
	res = re.sub(r'd\'', '', res)
	res = re.sub("nbsp", " ", res)
	res = res.replace("\\r\\n", " ")
	res = re.sub("[^a-zA-Z0-9\-_]", " ", res)  # remove all char except letters
	res = re.sub(r' (\w)\1{1,} ', '', res)  # remove words like pppp cc dd
	res = re.sub(" l ", " ", res)
	res = re.sub(r' +', ' ', res)  # remove irelevant spaces
	res = res.rstrip().lstrip()  # remove left and right spaces
	return str(res)

def stem(tags, language = "french"):
	#Discostemmer = SnowballStemmer(language, ignore_stopwords=True)
	stop = get_stop_words('fr') + get_stop_words('en')
	Discostemmer = SnowballStemmer(language, ignore_stopwords=True)
	if(type(tags) == str):
		stemmed = ''
		if tags in stop:	#tags is a stop word
			return ''

		if tags.find(' ') != -1:		#tags contains more than 1 word
			tmp = tags.split(' ')
			for t in tmp:
				if t not in stop :
					stemmed = stemmed + cleanSentence(Discostemmer.stem(t)) +' '
			stemmed = stemmed.rstrip()
		else:
				stemmed = cleanSentence(Discostemmer.stem(tags))
	else:
		stemmed = []
		for t in tags:
			if t not in stop:
				if t.find(' ') != -1:  # tags contains more than 1 word
					tsplit = t.split(' ')
					tmp = ''
					for w in tsplit:
						if w not in stop and Discostemmer.stem(w) != '':
							tmp = tmp + cleanSentence(Discostemmer.stem(w)) + ' '
					if tmp != '':
						stemmed.append(tmp.rstrip())
				else:
					if cleanSentence(Discostemmer.stem(t)) != '':
						stemmed.append(cleanSentence(Discostemmer.stem(t)))
	return stemmed

if __name__ == "__main__":
	Algostemmer = PorterStemmer()
	Discostemmer = SnowballStemmer("english", ignore_stopwords=True)

	tags = ['testing', 'C', 'C++', 'scala', 'Marketing director', 'Front-End',
	        'développeur web', 'développeur', 'developpement', 'développeuse', '#hello', 'developer', 'development']
	stemmed = [stem(t) for t in tags]
	print('First test with algoStemmer: ')
	print(stemmed)

	stemmed = [stem(t) for t in tags]
	print('Second test with DicoStemmer: ')
	print(stemmed)
	print()
	dic = {'café': 100.0, 'tasse': 97.17, 'tasses': 84.85, 'tasse de café': 83.72, 'thé': 81.69,
	        'espresso': 76.82, 'expresso': 74.93, 'boisson': 71.22, 'boissons': 69.95, 'tasses de café': 68.17}
	print(dic.keys(),np.unique(stem(list(dic.keys()))))

